
param(
    [string]$Path
)

if($null -eq $Path){
    $Path=$PWD
}

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Git"

Push-Location
Set-Location $Path
Get-CurrentGitDir
Pop-Location
