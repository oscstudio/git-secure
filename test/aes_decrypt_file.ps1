
param(
    [string]$File,
    [string]$Dest
)

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/AesProvider"

$aeskey=Read-Host "Please input aes key"

Restore-AesFile  -File "$File" -Key $aeskey -Destination $Dest
