# show log
$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Git"
Import-Module -Name "$PrefixDir/modules/Process"


$result=ProcessArgv -FilePath git -ArgumentList $args
exit $result
