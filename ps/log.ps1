# show log
$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Git"
Import-Module -Name "$PrefixDir/modules/Process"

$gitdir = Get-CurrentGitDir

$result=ProcessArgvDir -FilePath git -ArgumentList $args -Workdir "$gitdir/secure"
exit $result
