

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Git"
Import-Module -Name "$PrefixDir/modules/Process"

$gitdir = Get-CurrentGitDir

if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}


$result = ProcessArgvDir -FilePath git -ArgumentList $args -Workdir "$gitdir/secure"
exit $result
