### push to remote

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Git"
Import-Module -Name "$PrefixDir/modules/Process"

$gitdir = Get-CurrentGitDir

$pointer = Get-Content -ErrorAction Ignore -Path "$gitdir/pointer.json"|ConvertFrom-Json

if ($null -eq $pointer) {
    Write-Host -ForegroundColor Red "Not found secure pointer file,Please run git-secure commit ?"
    exit 1
}

$wid = git --git-dir=`"$gitdir`" rev-parse HEAD

if ($wid -ne $pointer.wid) {
    Write-Host -ForegroundColor Yellow "Warning: Please run git-secure commit"
}

exit (ProcessArgvDir -FilePath "git" -ArgumentList $args -Workdir "$gitdir/secure")
