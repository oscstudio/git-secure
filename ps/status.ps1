# show log
$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/modules/Process"

exit (ProcessArgv -FilePath git -ArgumentList $args)
